# avxinspect


## Getting started

Those scripts require awk, binutils, elfutils and rpm2cpio.


## Usage

The scripts can either look into locally installed packages or inspect .rpm
packages (extracting the content into some /tmp directory).
Disassembled code is stored in a /tmp directory too.

Since the tools want to pinpoint the functions that include some AVX
instructions, debug info packages are necessary.

The main script is analyse_rpm.sh which will inspect its content and write a
.report file. Since we don't want to analyse debuginfo/debugsource rpm content,
only the binary rpm name / file must be passed to this script.

Example:
```console
$ wget 'https://kojipkgs.fedoraproject.org//packages/dpdk/22.11.1/1.fc38/x86_64/dpdk-22.11.1-1.fc38.x86_64.rpm'
$ wget 'https://kojipkgs.fedoraproject.org//packages/dpdk/22.11.1/1.fc38/x86_64/dpdk-debuginfo-22.11.1-1.fc38.x86_64.rpm'
$ wget 'https://kojipkgs.fedoraproject.org//packages/dpdk/22.11.1/1.fc38/x86_64/dpdk-debugsource-22.11.1-1.fc38.x86_64.rpm'
$ ./analyse_rpm.sh dpdk-22.11.1-1.fc38.x86_64.rpm
```
