#!/bin/sh -e

for pkg in $@
do
	if [ -f $pkg ]; then
		name=$(rpm -q -p $pkg)
		dir=/tmp/avx512_objdump/rpm_content/$name
		mkdir -p $dir
		rpm2cpio $pkg | cpio -idm -D $dir 2>/dev/null
		files=$(find $dir)
		debug=$(rpm -q --queryformat '%{NAME}-debuginfo-%{VERSION}-%{RELEASE}.%{ARCH}.rpm\n' -p $pkg)
		if [ -e $debug ]
		then
			rpm2cpio $debug | cpio -idm -D $dir 2>/dev/null
		else
			echo "No debuginfo for $pkg"
		fi
		debug=$(rpm -q --queryformat '%{NAME}-debugsource-%{VERSION}-%{RELEASE}.%{ARCH}.rpm\n' -p $pkg)
		if [ -e $debug ]
		then
			rpm2cpio $debug | cpio -idm -D $dir 2>/dev/null
		else
			echo "No debugsource for $pkg"
		fi
	else
		name=$(rpm -q $pkg)
		files=$(rpm -ql $pkg)
		debug=$(rpm -q --queryformat '%{NAME}-debuginfo-%{VERSION}-%{RELEASE}.%{ARCH}\n' $pkg)
		rpm -q $debug >/dev/null || echo "No debuginfo for $pkg"
		debug=$(rpm -q --queryformat '%{NAME}-debugsource-%{VERSION}-%{RELEASE}.%{ARCH}\n' $pkg)
		rpm -q $debug >/dev/null || echo "No debugsource for $pkg"
	fi
	./analyse_file.sh /tmp/avx512_objdump/$name $files >$name.report
	if [ -f $pkg ]; then
		rm -rf $dir
	fi
done
